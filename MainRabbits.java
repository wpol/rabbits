package rabbits;

public class MainRabbits {
    public static void main(String[] args) {
        int maleCount = 10;
        int femaleCount = 10;
        long limit = 10000;

        RabbitsSimulation rabbitsSimulation = new RabbitsSimulation(maleCount, femaleCount, limit);
        System.out.println(rabbitsSimulation.simulate(maleCount, femaleCount, limit));
    }
}
