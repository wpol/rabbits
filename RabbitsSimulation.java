package rabbits;

import java.util.ArrayList;
import java.util.List;

public class RabbitsSimulation {
    private int maleCount;
    private int femaleCount;
    private long limit;
    private final static int longLive = 96;

    public RabbitsSimulation(int maleCount, int femaleCount, long limit) {
        this.maleCount = maleCount;
        this.femaleCount = femaleCount;
        this.limit = limit;
    }

    List<Rabbit> male = new ArrayList<>();
    List<Rabbit> female = new ArrayList<>();

    public int simulate(int maleCount, int femaleCount, long limit) {
        int months = 0;
        init();
        do {
            allAge();
            breed();
            death();
            months++;
        }
        while (getNumber() < limit);
        return months;
    }

    private void init() {
        for (int i = 0; i < maleCount; i++) {
            male.add(new Rabbit());
        }
        for (int i = 0; i < femaleCount; i++) {
            female.add(new Rabbit());
        }
    }

    private void breed() {
        int maleToBirth = 0;
        int femaleToBirth = 0;

        for (Rabbit rabbit : female) {
            if (rabbit.isFertile()) {
                maleToBirth += 5;
                femaleToBirth += 14;
            }
        }
        birth(male, maleToBirth);
        birth(female, femaleToBirth);
    }

    private void birth(List<Rabbit> rabbit, int countBirth) {
        for (int i = 0; i < countBirth; i++) {
            rabbit.add(new Rabbit());
        }
    }

    private void allAge() {
        for (Rabbit rabbit : male) {
            rabbit.age();
        }
        for (Rabbit rabbit : female) {
            rabbit.age();
        }
    }

    private void death() {
        for (Rabbit rabbit : male) {
            if (rabbit.getAge() > longLive)
                male.remove(rabbit);
        }
        for (Rabbit rabbit : female) {
            if (rabbit.getAge() > longLive)
                female.remove(rabbit);
        }
    }

    private int getNumber() {
        return male.size() + female.size();
    }
}
